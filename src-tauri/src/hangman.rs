use dotenv::dotenv;
use rand::Rng;
use serde::ser::{Serialize, SerializeStruct, Serializer};
use serde_json::json;
use std::cell::RefCell;
use std::fmt::Formatter;
use std::fs::File;
use std::io::Read;
use std::option::Option;
use std::{env, fmt, vec};

pub(crate) struct Hangman {
    word_to_guess: RefCell<String>,
    lives: RefCell<u8>,
    guessed: RefCell<LetterList>,
}

struct LetterList {
    characters: Vec<char>,
}

impl LetterList {
    fn new(characters: Vec<char>) -> Self {
        LetterList { characters }
    }
}

impl Serialize for LetterList {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut s = serializer.serialize_struct("LetterList", 1)?;
        s.serialize_field("characters", &self.characters)?;
        s.end()
    }
}

impl Hangman {
    pub(crate) fn new() -> Self {
        Hangman {
            word_to_guess: RefCell::new("".into()),
            lives: RefCell::new(0),
            guessed: RefCell::new(LetterList::new(vec!['_'; 1])),
        }
    }
    pub(crate) fn start_game(&self, word_to_guess: Option<&str>)->String {
        let word: String = match word_to_guess {
            Some(word_to_guess) if word_to_guess == "\n" => get_word_from_file(),
            Some(word_to_guess) => word_to_guess.to_string(),
            None => get_word_from_file(),
        };
        *self.guessed.borrow_mut() = LetterList::new(vec!['_'; word.trim().chars().count()]);
        *self.lives.borrow_mut() = 10;
        *self.word_to_guess.borrow_mut() = word;
        println!("{}", *self.word_to_guess.borrow());
        self.return_json()

    }
    fn return_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
    pub(crate) fn guess(&mut self, input: Option<&str>)->String {
        if input.is_some() {
            let character = input.unwrap().chars().nth(0).unwrap();
            if self.word_to_guess.borrow().contains(character) {
                self.reveal_character(character);
            }else {
                *self.lives.borrow_mut() = *self.lives.borrow() - 1;
            }
        }
        self.return_json()

    }
    fn reveal_character(&mut self, character: char) {
        for i in 0..self.word_to_guess.borrow().len() {
            if self.word_to_guess.borrow().chars().nth(i) == Some(character) {
                self.guessed.borrow_mut().characters[i] = character;
            }
        }
    }
}
impl fmt::Display for LetterList {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.characters.iter().fold(Ok(()), |result, letter| {
            result.and_then(|_| write!(f, "{} ", letter))
        })
    }
}
impl fmt::Display for Hangman {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let value = json!({
            "lives": self.lives,
            "guessed": self.guessed
        });
        write!(f, "{}", value)
    }
}
impl Serialize for Hangman {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut s = serializer.serialize_struct("Hangman", 2)?;
        //s.serialize_field("word_to_guess", &self.word_to_guess)?;
        s.serialize_field("lives", &self.lives)?;
        s.serialize_field("guessed", &self.guessed)?;
        s.end()
    }
}

/*fn main() {
    dotenv().ok();
    let word: String = startup();
    let mut game = Hangman::new();
    game.start_game(Some(&word));
    while game.lives.borrow() > 0 && game.guessed.characters.contains(&'_') {
        process::Command::new("clear").status().unwrap();
        println!("{}", game);
        game.guess(Some(&*ask_character()));
    }
    if game.lives.borrow() == 0 {
        println!(
            "Defeat, the word you where looking for was: {}",
            game.word_to_guess.borrow()
        );
    } else {
        println!("Victory, you fount the word: {}", game.word_to_guess.borrow());
    }
}*/

/*fn startup() -> String {
    let mut input = String::new();
    println!("Give a word to start hangman (or just press enter if you want to get a random word)");
    io::stdin()
        .read_line(&mut input)
        .expect("failed to read line");
    process::Command::new("clear").status().unwrap();
    input
}

fn ask_character() -> String {
    let mut input = String::new();
    println!("Give a letter to try");
    io::stdin()
        .read_line(&mut input)
        .expect("failed to read line");
    input
}
*/
fn get_word_from_file() -> String {
    dotenv().ok();
    let mut file = File::open(env::var("WORDLIST").expect("Could not read env file"))
        .expect("Could not open file");
    let mut words = String::new();
    file.read_to_string(&mut words)
        .expect("An error occured while reading the file");
    let available_words: Vec<&str> = words.trim().split('\n').collect();
    let random_index = rand::thread_rng().gen_range(0..available_words.len());
    String::from(available_words[random_index])
}
