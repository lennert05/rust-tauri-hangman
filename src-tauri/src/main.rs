#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use std::borrow::Borrow;
use crate::hangman::Hangman;
use std::sync::Mutex;
pub mod hangman;

struct GameState{
    state: Mutex<Hangman>
}

fn main() {
    let context = tauri::generate_context!();
    tauri::Builder::default()
        .manage(GameState{state: Mutex::new(Hangman::new())})
        .invoke_handler(tauri::generate_handler![testing, new_game, guess])
        //.menu(tauri::Menu::os_default(&context.package_info().name))
        .run(context)
        .expect("error while running tauri application");
}

#[tauri::command]
fn testing() {
    println!("testing from js");
}

#[tauri::command]
fn new_game(word: Option<&str>, game: tauri::State<GameState>)-> String{
    game.state.lock().unwrap().start_game(word)
}

#[tauri::command]
fn guess(letter: Option<&str>, game: tauri::State<GameState>)-> String{
    game.state.lock().unwrap().guess(letter)
}
